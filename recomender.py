import functools
import numpy as np
import operator

from owlready2 import *

activityIds = np.loadtxt('activityIDs.txt',dtype=str,encoding='latin1')


def generateRecomendationList(user_id,user_data):
    try:
        matrix = np.load(str(user_id)+'.npy')#.reshape(-1,21)
    except Exception as inst:
        matrix = np.array([])
    restrictionSet = preprocess(user_data)
    recomendationStr = []
    recomendation = []
    onto = get_ontology("file://ActivitiesOntology.owl").load()

    if "ActivitiesOntology.Subset" not in list(onto.classes()):
        class Subset(onto.Activity):
            equivalent_to = [onto.Activity]

    restriction = buildRestriction(onto, restrictionSet)
    onto.Subset.equivalent_to = [restriction]
    print(onto.Subset.equivalent_to)
    #close_world(onto.Activity)
    with onto: sync_reasoner()
    for i in onto.search(subclass_of = onto.Subset):
        recomendationStr.append(str(i))
        recomendation.append(i)

    print(recomendationStr)
    if len(recomendation) == 0:
        return recomendation
    np.random.shuffle(recomendation)
    rComparable = makeComparable(recomendation, matrix)
    if getPhase(matrix,onto) == 0:   #standard frequency from ontology more important
        ind = np.lexsort(keys=(rComparable[:, 1],rComparable[:, 2]),axis=0)
    else:                       #user feedback more important
        ind = np.lexsort(keys=(rComparable[:, 2], rComparable[:, 1]),axis=0)
    rComparable = np.flip(rComparable[ind],axis=0)     #to get highest first
    onto = clearFromSubsetAsParent(onto)
    return rComparable[:,0]


def buildRestriction(onto, restrictionSet):
    restriction = onto.Activity & onto.hasFrequency.min(1,onto.Frequency)   #min-restriction to avoid super-classes like 'Sports' that do not have cardinality
    for category in restrictionSet:
        choice = restrictionSet[category]
        if category == "Motivation":
            r = onto.hasMotivation.some(onto[choice])
        elif category == "Duration":
            r = onto.hasDuration.some(onto[choice])
        elif category == "People":
            r = onto.requires.some(onto[choice])
        elif category == "Daytime":
            r = onto.requires.some(onto[choice])
        elif category == "Place":
            if category == "Home":
                r = onto.hasPlace.some(onto[choice])
            else:
                #r = Not(onto.hasPlace.some(onto.Home))
                r = onto.hasPlace.some(Not(onto.Home))
        restriction = restriction & r
    return restriction

def makePrintable(recomendation):
    recomStr = []
    for entry in recomendation:
        recomStr.append(act_to_str(entry,True))
    return recomStr

def act_to_str(act,user_readable):
    string = str(act)[19:]  #Cut away 'ActivitiesOntology.'
    if user_readable:
        string = string.replace('_',' ').lower()
    return string

def preprocess(user_data):
    if 'Daytime' in user_data.keys():
        if isinstance(user_data['Daytime'], datetime.datetime):
            now = user_data['Daytime'].time()
            if now > datetime.time(hour=20):
                user_data['Daytime'] = 'Evening'
            else:
                user_data['Daytime'] = 'Day'
    if 'People' in user_data.keys():
        if user_data['People'] == 'Alone':
            user_data['People'] = 'Individually'
    return user_data

def cmp_to_key(mycmp,matrix):
    'Convert a cmp= function into a key= function'
    class K(owlready2.owl_class):
        def __init__(self, obj, *args):
            self.obj = obj
        def __lt__(self, other,matrix):
            return mycmp(self.obj, other.obj,matrix) < 0
        def __gt__(self, other,matrix):
            return mycmp(self.obj, other.obj,matrix) > 0
        def __eq__(self, other,matrix):
            return mycmp(self.obj, other.obj,matrix) == 0
        def __le__(self, other,matrix):
            return mycmp(self.obj, other.obj,matrix) <= 0
        def __ge__(self, other,matrix):
            return mycmp(self.obj, other.obj,matrix) >= 0
        def __ne__(self, other,matrix):
            return mycmp(self.obj, other.obj,matrix) != 0
    return K

def makeComparable(recomendation, matrix):
    r = []
    order = ['Never','Rarely','Sometimes','Frequent','Often','Always']
    for element in recomendation:
        for thing in element.is_a:
            if isinstance(thing, Restriction) and thing.property.name == 'hasFrequency':
                freq = order.index(act_to_str(thing.value,False))
                r.append([element, getFeedback(matrix, element),freq])
                break
    return np.array(r)

def getFeedback(matrix, activity):
    act = act_to_id(act_to_str(activity,False))
    index = np.where(matrix[:,0] == act)
    if len(matrix) == 0 or len(index) == 0:
        return 0
    return np.sum(matrix[index[0],1:])

def saveFeedback(user, act, positive):
    activity = act_to_id(act_to_str(act,False))
    if positive:
        feedback = 1
    else:
        feedback = -0.6
    new = [[activity, feedback, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    try:
        fArray = np.load(str(user)+'.npy').reshape(-1,21)
    except FileNotFoundError:
        fArray = np.array(new)
    except Exception as inst:
        print(inst)
    else:
        if fArray.shape == (21,):
            fArray.reshape == (1,21)
        indexActivity = np.where(fArray[:,0] == activity)
        if len(indexActivity[0]) == 0:
            fArray = np.append(fArray,new,axis=0)
        else:
            fArray[indexActivity[0],1:] = np.roll(fArray[indexActivity[0],1:],-1)
            fArray[indexActivity[0],1] = feedback
    finally:
        np.save(str(user)+'.npy',fArray)

def getPhase(matrix, onto):
    if len(matrix) == 0:
        return 0
    nonZeros = np.count_nonzero(matrix)-matrix.shape[0]
    total = len(list(onto.classes()))*20
    if nonZeros/total > 0.5:
        return 1
    else:
        return 0

def act_to_id(act):
    global activityIds
    length = len(activityIds)
    index = activityIds.tolist().index(act)
    if length == 0 or index == -1:
        np.savetxt('activityIDs.txt',np.append(activityIds,act),fmt='<U28')
        return length
    else:
        return index

def clearFromSubsetAsParent(onto):
    for c in onto.classes():
        for thing in c.is_a:
            if isinstance(thing, ThingClass) and thing.name == 'Subset':
                c.is_a.remove(thing)
    return onto