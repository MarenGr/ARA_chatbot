#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to reply to Telegram messages
# This program is dedicated to the public domain under the CC0 license.
"""
This Bot uses the Updater class to handle the bot.
First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

from telegram import ReplyKeyboardMarkup
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)
import datetime
import recomender as r

import logging

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

WORK, MOTIVATION, CHOOSING, DURATION, PEOPLE, PLACE, DAYTIME, ACCEPTION = range(8)

back = 'Not Important. Back Please'

decision_keyboard = [['Yes', 'No']]
decisionAdv_keyboard = [['Yes', 'No'],['I would like to change some settings.']]
work_keyboard = [['Yes', "No, thanks for the reminder"]]
motivation_keyboard = [['Relax','Normal','Action']]
else_keyboard = [['Duration', 'Place',],
                 ['Daytime', 'People'],
                  ['Done']]
duration_keyboard = [['Short','Medium','Long'],[back]]
people_keyboard = [['Alone','Group'],[back]]
place_keyboard = [['Home','Out'],[back]]
daytime_keyboard = [['Now','Day','Evening'],[back]]

decision_markup = ReplyKeyboardMarkup(decision_keyboard, one_time_keyboard=True)
decisionAdv_markup = ReplyKeyboardMarkup(decisionAdv_keyboard, one_time_keyboard=True)
work_markup = ReplyKeyboardMarkup(work_keyboard, one_time_keyboard=True)
motivation_markup = ReplyKeyboardMarkup(motivation_keyboard, one_time_keyboard=True)
else_markup = ReplyKeyboardMarkup(else_keyboard, one_time_keyboard=True)
duration_markup = ReplyKeyboardMarkup(duration_keyboard, one_time_keyboard=True)
people_markup = ReplyKeyboardMarkup(people_keyboard, one_time_keyboard=True)
place_markup = ReplyKeyboardMarkup(place_keyboard, one_time_keyboard=True)
daytime_markup = ReplyKeyboardMarkup(daytime_keyboard, one_time_keyboard=True)


recomendationCount = 0
recomendationList = []

def facts_to_str(user_data):
    facts = list()

    for key, value in user_data.items():
        if isinstance(value, datetime.datetime):
            facts.append('{} - {}'.format(key,'Now'))
        else:
            facts.append('{} - {}'.format(key, value))

    return "\n".join(facts).join(['\n', '\n'])

def start(bot, update):
    update.message.reply_text(
        "Hi! My name is ARA. Do you want me to find an activity for you? I am happy to help :-)\n"
        "But first. Do you have all your chores done?",
        reply_markup=decision_markup)

    return WORK

def work_choice(bot, update, user_data):
    text = update.message.text
    #TODO:Handle switch case
    if text == "Yes":
        update.message.reply_text(
            'Okay then.\n'
            "Let's start with your energy level. What are you up for?", reply_markup=motivation_markup)
        user_data['choice'] = "Motivation"
        return MOTIVATION
    elif text == "No":
        update.message.reply_text(
            'Then let us do those first ;)\n'
            "I only help you now if you insist. Do you?", reply_markup=work_markup)
        return WORK
    elif text == "No, thanks for the reminder":
        update.message.reply_text(
            'On to work!\n'
            "See you afterwards :)")
        return ConversationHandler.END

def regular_choice(bot, update, user_data):
    text = update.message.text
    if text == back:
        if user_data['choice'] in user_data:
            del user_data[user_data['choice']]
        del user_data['choice']
    elif not text == decisionAdv_keyboard[1][0]:
        category = user_data['choice']
        if text == 'Now':
            user_data[category] = update.message.date
        else:
            user_data[category] = text
        del user_data['choice']

    update.message.reply_text(
        'Alright!\n'
        "Just so you know, this is what you already told me: "
        "{}"
        "You can tell me more, or change your opinion on something.".format(facts_to_str(user_data)),reply_markup=else_markup)

    return CHOOSING

def duration(bot, update, user_data):
    text = update.message.text
    user_data['choice'] = text
    update.message.reply_text(
        "How much time do you have?", reply_markup=duration_markup)
    return DURATION

def people(bot, update, user_data):
    text = update.message.text
    user_data['choice'] = text
    update.message.reply_text(
        "Are you alone or with friends?", reply_markup=people_markup)
    return PEOPLE

def place(bot, update, user_data):
    text = update.message.text
    user_data['choice'] = text
    update.message.reply_text(
        "Would you rather stay at home or go outside", reply_markup=place_markup)
    return PLACE

def daytime(bot, update, user_data):
    text = update.message.text
    user_data['choice'] = text
    update.message.reply_text(
        "For when are you searching an activity?", reply_markup=daytime_markup)
    return DAYTIME


def bye(bot, update, user_data):
    update.message.reply_text("So glad I could help! "
                              "See you next time :)")
    user = update.message.from_user.id
    r.saveFeedback(user,recomendationList[recomendationCount-1],True)
    clear(user_data)
    return ConversationHandler.END

def clear(user_data):
    recomendationCount = 0
    recomendationList = []
    user_data.clear()


def recomend(bot, update, user_data):
    global recomendationCount
    global recomendationList
    if 'choice' in user_data:
        del user_data['choice']

    text = update.message.text
    if text == "Done":
        update.message.reply_text("Hm, let me think.")
        user = update.message.from_user.id
        recomendationList = r.generateRecomendationList(user,user_data)

    if recomendationCount < len(recomendationList):
        if text == 'No':
            user = update.message.from_user.id
            r.saveFeedback(user,recomendationList[recomendationCount-1],False)
        update.message.reply_text("Do you feel like " + r.act_to_str(recomendationList[recomendationCount],True) + "?",
                                  reply_markup=decisionAdv_markup)
        recomendationCount += 1
        return ACCEPTION
    else:
        update.message.reply_text("I am sorry, I cannot find any activity with the following properties:\n"
                                "{}"
                                "\nPlease change some of your selections.".format(facts_to_str(user_data)),
                                  reply_markup=else_markup)
        recomendationCount = 0
        return CHOOSING


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    # Create the Updater and pass it your bot's token.
    updater = Updater("562912074:AAHns7uKeG2cKQFJaaSp6q3B5QYZ8U_lKiE")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Add conversation handler with the above defined states
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],

        states={
            WORK: [MessageHandler(Filters.text, work_choice, pass_user_data=True)],
            MOTIVATION: [MessageHandler(Filters.text, regular_choice, pass_user_data=True)],
            CHOOSING: [ RegexHandler('^(Duration)$', duration, pass_user_data=True),
                        RegexHandler('^(People)$', people, pass_user_data=True),
                        RegexHandler('^(Place)$', place, pass_user_data=True),
                        RegexHandler('^(Daytime)$', daytime, pass_user_data=True)],
            DURATION: [MessageHandler(Filters.text, regular_choice, pass_user_data=True)],
            PEOPLE: [MessageHandler(Filters.text, regular_choice, pass_user_data=True)],
            PLACE: [MessageHandler(Filters.text, regular_choice, pass_user_data=True)],
            DAYTIME: [MessageHandler(Filters.text, regular_choice, pass_user_data=True)],
            ACCEPTION: [RegexHandler('^(No)$', recomend, pass_user_data=True),
                        RegexHandler('^(Yes)$', bye, pass_user_data=True),
                        RegexHandler('^('+decisionAdv_keyboard[1][0]+')$',regular_choice, pass_user_data=True)]
        },

        fallbacks=[RegexHandler('^Done$', recomend, pass_user_data=True)]
    )

    dp.add_handler(conv_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
	main()